import urllib2
import json

from django.shortcuts import render, get_object_or_404, redirect#, render_to_response
from django.http import HttpResponse, Http404
from django.template import RequestContext, Template
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.contrib import sessions
#from django.core.context_processors import csrf

from GGUser.models import GGUser
from Blog.models import News

from GGUser.forms import SignupForm, LoginForm, SettingsForm
from Blog.forms import NotecardForm

# handles LOGIN requests
def login_view(request):
    if request.user.is_authenticated():
        return redirect('profile_smart')
    var = {}
    # var['news_contents'] = News.objects.all().order_by('-last_edited')[:5]
    # login form
    login_form = LoginForm()
    request.session['last_tab'] = None
    if request.method == "POST":
        request.session['last_tab'] = 'auth'
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            username = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('profile_smart')
    var['login_form'] = login_form
    if login_form['username'].errors:
        request.session['login_error'] = login_form['username'].errors[0]
    if login_form['password'].errors:
        request.session['login_error'] = login_form['password'].errors[0]
    return redirect('profile_smart')

# signup
def signup(request):
    signup_form = SignupForm()
    request.session['last_tab'] = None
    if request.method == "POST":
        request.session['last_tab'] = 'signup'
        signup_form = SignupForm(request.POST)
        if signup_form.is_valid():
            username = signup_form.cleaned_data['username']
            email = signup_form.cleaned_data['email']
            password = signup_form.cleaned_data['password']
            password_conf = signup_form.cleaned_data['password_conf']
            name = signup_form.cleaned_data['name']
            auth_user = User.objects.create_user(username=username,
                                                email=email,
                                                password=password,
                                                first_name=name)
            GGUser.objects.create(auth_user=auth_user)
            auth_user = authenticate(username=username, password=password)
            login(request, auth_user)
            return redirect('profile_smart')
    var = {}
    var['signup_form'] = signup_form
    if signup_form['username'].errors:
        request.session['signup_username_error'] = signup_form['username'].errors[0]
    if signup_form['email'].errors:
        request.session['signup_email_error'] = signup_form['email'].errors[0]
    if signup_form['name'].errors:
        request.session['signup_name_error'] = signup_form['name'].errors[0]
    if signup_form['password'].errors:
        request.session['signup_password_error'] = signup_form['password'].errors[0]
    if signup_form['password_conf'].errors:
        request.session['signup_password_conf_error'] = signup_form['password_conf'].errors[0]
    return redirect('profile_smart')

# change settings in the profile panel
def settings_submit(request):
    if request.method == "POST":
        form = SettingsForm(request.POST, request.FILES)
        if form.is_valid():
            auth_user = request.user
            user = GGUser.objects.get(auth_user=auth_user)
            if 'new_name' in form.cleaned_data:
                auth_user.first_name = form.cleaned_data['new_name']
            if 'new_password_conf' in form.cleaned_data:
                auth_user.set_password(form.cleaned_data['new_password_conf'])
            if 'new_prof_pic' in form.cleaned_data:
                user.image = request.FILES.get('new_prof_pic', False)
            auth_user.save()
            user.save()
            return redirect('profile_smart')
    return redirect('profile_smart')

# logout
def logout_view(request):
    logout(request)
    return redirect('profile_guest')

# view the main page without specifying guest/ GGUser_id
def view_main_smart(request):
    if not request.user.is_authenticated():
        return redirect('profile_guest')
    else:
        user = get_object_or_404(GGUser, auth_user_id=request.user.id)
        return redirect('/gguser/' + str(user.id))

# view the main page as an authenticated GGUser
def view_main(request, GGUser_id):
    if not request.user.is_authenticated():
        return redirect('profile_guest')

    # get GGUser object with id GGUser_id
    user = get_object_or_404(GGUser, id=GGUser_id)

    if user.auth_user.id != request.user.id:
        return redirect('profile_smart')

    var = {}

    # define the output data as dictionary named var
    # send user data to frontend
    var['auth_user'] = user.auth_user
    var['user'] = user

    # Notecard form
    var['notecard_form'] = NotecardForm()

    # Settings form
    var['settings_form'] = SettingsForm()

    # render to main.html
    return render(request, 'GGUser/main.html', var)

# view the main page as a guest
def view_main_guest(request):
    # if logged in go to authenticated page
    if request.user.is_authenticated():
        return redirect('profile_smart')

    # define the output data as dictionary named var
    var = {}

    # login form
    login_form = LoginForm()
    if 'login_error' in request.session:
        var['login_error'] = 'Invalid username or password'
        request.session.pop('login_error', None)
    var['login_form'] = login_form

    # signup form
    signup_form = SignupForm()
    if 'signup_username_error' in request.session:
        var['signup_username_error'] = request.session.pop('signup_username_error')
    if 'signup_email_error' in request.session:
        var['signup_email_error'] = request.session.pop('signup_email_error')
    if 'signup_name_error' in request.session:
        var['signup_name_error'] = request.session.pop('signup_name_error')
    if 'signup_password_error' in request.session:
        var['signup_password_error'] = request.session.pop('signup_password_error')
    if 'signup_password_conf_error' in request.session:
        var['signup_password_conf_error'] = request.session.pop('signup_password_conf_error')
    var['signup_form'] = signup_form

    # check last tab and activate last tab on reload
    if 'last_tab' in request.session:
        var['last_tab'] = request.session.pop('last_tab')

    # render to main.html
    return render(request, 'GGUser/main.html', var)

# respond to an ajax GET request with a list of colleagues (colleagues that the
# user is following is marked as such)
def get_colleagues_ajax(request, GGUser_id):
    # check if request is ajax and respond accordingly
    if request.is_ajax():
        user = get_object_or_404(GGUser, id=GGUser_id)
        json_response_data = []
        following_colleagues = user.colleagues.all()
        all_colleagues = GGUser.objects.all(
                            ).exclude(id=GGUser_id
                            ).order_by('auth_user__first_name')
        for colleague in all_colleagues:
            colleague_dict = {}
            colleague_dict['id'] = colleague.id
            colleague_dict['username'] = colleague.auth_user.username
            colleague_dict['name'] = colleague.auth_user.first_name
            colleague_dict['email'] = colleague.auth_user.email
            colleague_dict['prof_pic'] = str(colleague.profile_pic)
            if colleague in following_colleagues:
                colleague_dict['following'] = "True"
            json_response_data.append(colleague_dict)
        return HttpResponse(json.dumps(json_response_data), content_type="application/json")

# respond to an ajax POST request by adding the GGUser with the followee_id to
# colleagues field of the GGUser with the GGUser_id
def follow_colleague_ajax(request, GGUser_id, followee_id):
    # check if request is ajax AND POST and respond accordingly
    if request.is_ajax() and request.method == 'POST':
        if GGUser_id != followee_id:
            # get GGUser object with id GGUser_id
            user = get_object_or_404(GGUser, id=GGUser_id)
            followee = get_object_or_404(GGUser, id=followee_id)
            user.colleagues.add(followee)
            # CSRF
            c = RequestContext(request, {})
            t = Template("SUCCESS")
            return HttpResponse(t.render(c), content_type='text/html')

# respond to an ajax POST request by removing the GGUser with the followee_id
# from colleagues field of the GGUser with the GGUser_id
def unfollow_colleague_ajax(request, GGUser_id, followee_id):
    # check if request is ajax AND POST and respond accordingly
    if request.is_ajax() and request.method == 'POST':
        if GGUser_id != followee_id:
            # get GGUser object with id GGUser_id
            user = get_object_or_404(GGUser, id=GGUser_id)
            followee = get_object_or_404(GGUser, id=followee_id)
            user.colleagues.remove(followee)
            # CSRF
            c = RequestContext(request, {})
            t = Template("SUCCESS")
            return HttpResponse(t.render(c), content_type='text/html')