from django.db import models
from django.contrib.auth.models import User

class GGUser(models.Model):
	'''
	represents a normal user of the GeoGateway site. GGUser has an instance of
	Django's auth_user. GG stands for GeoGateway
	'''

	def __unicode__(self):
		return self.auth_user.username

	# auth_user is provided by Django
	auth_user = models.OneToOneField(User)

	profile_pic = models.ImageField(
		upload_to="profile_pics",
		blank=True,
		editable=True)

	# colleagues that a user is following
	colleagues = models.ManyToManyField(
		"GGUser",
		blank=True)