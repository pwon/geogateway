import re

from django import forms
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from GGUser.models import GGUser
from Blog.models import Notecard

class LoginForm(forms.Form):
	username = forms.CharField(
		max_length=30,
		label=u'Username',
		widget=forms.TextInput({'class': 'form-control', 'placeholder': 'Username'}))
	password = forms.CharField(
		label=u'Password',
		widget=forms.PasswordInput({'class': 'form-control', 'placeholder': 'Password'}))

	error_msg = u'Invalid username or password'

	def clean(self):
		cleaned_data = self.cleaned_data
		username = cleaned_data.get('username')
		if cleaned_data.has_key('password'):
			password = cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				return cleaned_data
			del cleaned_data["password"]
			self._errors["username"] = self.error_class([self.error_msg])
		else:
			return cleaned_data

class SignupForm(forms.Form):
	username = forms.CharField(
		max_length=30,
		label=u'Username',
		widget=forms.TextInput({'class': 'form-control', 'placeholder': 'Username'}))
	email = forms.EmailField(
		label=u'Email',
		widget=forms.TextInput({'class': 'form-control', 'placeholder': 'exp@exp.com'}))
	name = forms.CharField(
		max_length=30,
		label=u'Full Name',
		widget=forms.TextInput({'class': 'form-control', 'placeholder': 'Joe Schmo'}))
	password = forms.CharField(
		label=u'Password',
		widget=forms.PasswordInput({'class': 'form-control', 'placeholder': 'Password'}))
	password_conf = forms.CharField(
		label=u'Password (repeat)',
		widget=forms.PasswordInput({'class': 'form-control', 'placeholder': 'Password (repeat)'}))

	def clean_username(self):
		username = self.cleaned_data['username']
		if username.__len__() < 4:
			raise forms.ValidationError(u'Username must be longer than 4 characters')
		if not re.search(r'^\w+$', username):
			raise forms.ValidationError(u'Username must only include alphanumeric characters and underscores')
		try:
			User.objects.get(username=username)
		except ObjectDoesNotExist:
			return username
		raise forms.ValidationError(u'Username already exists')

	def clean_email(self):
		email = self.cleaned_data['email']
		try:
			User.objects.get(email=email)
		except ObjectDoesNotExist:
			return email
		raise forms.ValidationError(u'User with this email already exists')

	def clean_password(self):
		password = self.cleaned_data['password']
		if password.__len__() >= 4:
			return password
		raise forms.ValidationError(u'Password must be longer than 4 characters')

	def clean_password_conf(self):
		if 'password' in self.cleaned_data:
			password = self.cleaned_data['password']
			password_conf = self.cleaned_data['password_conf']
			if password == password_conf:
				return password
			else:
				raise forms.ValidationError(u'Password repeat must match the first password')

class SettingsForm(forms.Form):
	new_name = forms.CharField(
		max_length=30,
		label=u'New Full Name',
		widget=forms.TextInput({'class': 'form-control', 'placeholder': 'New Name'}),
		required=False)

	new_password = forms.CharField(
		label=u'New Password',
		widget=forms.PasswordInput({'class': 'form-control', 'placeholder': 'New Password'}),
		required=False)

	new_password_conf = forms.CharField(
		label=u'New Password (repeat)',
		widget=forms.PasswordInput({'class': 'form-control', 'placeholder': 'New Password (repeat)'}),
		required=False)

	new_prof_pic = forms.ImageField(
		label='New Profile Picture')

	def clean_new_password(self):
		new_password = self.cleaned_data['new_password']
		if new_password.__len__() >= 4:
			return new_password
		raise forms.ValidationError(u'Password must be longer than 4 characters')

	def clean_new_password_conf(self):
		if 'new_password' in self.cleaned_data:
			new_password = self.cleaned_data['new_password']
			new_password_conf = self.cleaned_data['new_password_conf']
			if new_password == new_password_conf:
				return new_password
			else:
				raise forms.ValidationError(u'New Password repeat must match the first New Password')