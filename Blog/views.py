import urllib2
import json

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.template import RequestContext, Template
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User

from GGUser.models import GGUser
from Blog.models import Notecard

from Blog.forms import NotecardForm

# respond to an ajax request with most recent notecards to frontend as JSON
# num_cards specify the number of notecards to be returned
def get_notecards_ajax(request, GGUser_id, start_index, num_cards):
    # check if request is ajax and respond accordingly
    if request.is_ajax():
        num_cards = int(num_cards)
        start_index = int(start_index)
        GGUser_id = int(GGUser_id)
        user = get_object_or_404(GGUser, auth_user=request.user)
        json_response_data = []
        if GGUser_id == -1:
            notecards = Notecard.objects.filter(public=True).order_by('-last_edited')
            index = 0
            while index < len(notecards) and index < start_index + num_cards:
                if notecards[index].user not in user.colleagues.all() and notecards[index].user != user:
                    notecards = notecards[1:]
                else:
                    index += 1
        else:
            notecards = Notecard.objects.filter(user_id=GGUser_id).order_by('-last_edited')
        if len(notecards) >= start_index:
            notecards = notecards[start_index:]
        if len(notecards) >= num_cards:
            notecards = notecards[:num_cards]
        for notecard in notecards:
            notecard_dict = {}
            notecard_dict['id'] = notecard.id
            notecard_dict['last_editor'] = notecard.user.auth_user.first_name
            notecard_dict['title'] = notecard.title
            notecard_dict['content'] = notecard.content.replace('\n', '<br>')
            if notecard.public:
                notecard_dict['privacy'] = 'Public'
            else:
                notecard_dict['privacy'] = 'Private'
            image = str(notecard.image)
            if not (image == None or image == 'False' or image == ''):
                notecard_dict['image'] = str(notecard.image)
            notecard_dict['last_edited'] = notecard.last_edited.strftime('%Y-%m-%d %H:%M')
            json_response_data.append(notecard_dict)
        return HttpResponse(json.dumps(json_response_data), content_type="application/json")

# respond to an ajax post request by recording the new notecard in database
def post_notecard_ajax(request, GGUser_id):
    # check if request is ajax AND POST and respond accordingly
    if request.is_ajax() and request.method == 'POST':
        notecard_form = NotecardForm(request.POST, request.FILES)
        if notecard_form.is_valid():
            # get GGUser object with id GGUser_id
            user = get_object_or_404(GGUser, id=GGUser_id)
            title = notecard_form.cleaned_data['title']
            content = notecard_form.cleaned_data['content']
            image = request.FILES.get('image', False)
            new_notecard = Notecard(user=user, title=title, content=content, image=image)
            new_notecard.save()
            # CSRF
            c = RequestContext(request, {})
            t = Template("SUCCESS")
            return HttpResponse(t.render(c), content_type='text/html')
    # CSRF
    c = RequestContext(request, {})
    t = Template("FAIL")
    return HttpResponse(t.render(c), content_type='text/html')

# respond to an ajax post request by deleting the specified (card_id) notecard
def delete_notecard_ajax(request, GGUser_id, card_id):
    # check if request is ajax AND POST and respond accordingly
    if request.is_ajax() and request.method == 'POST':
        card = get_object_or_404(Notecard, id=card_id)
        user = get_object_or_404(GGUser, id=GGUser_id)
        # make sure the requestor is the owner of the notecard
        if user.auth_user.id == request.user.id and card.user == user:
            card.delete()
        # CSRF
        c = RequestContext(request, {})
        t = Template("SUCCESS")
        return HttpResponse(t.render(c), content_type='text/html')
    # CSRF
    c = RequestContext(request, {})
    t = Template("FAIL")
    return HttpResponse(t.render(c), content_type='text/html')

# respond to an ajax post request by making the specified notecard (card_id)
# public or private
def publish_notecard_ajax(request, GGUser_id, card_id, type):
    # check if request is ajax AND POST and respond accordingly
    if request.is_ajax() and request.method == 'POST':
        card = get_object_or_404(Notecard, id=card_id)
        user = get_object_or_404(GGUser, id=GGUser_id)
        # make sure the requestor is the owner of the notecard
        if user.auth_user.id == request.user.id and card.user == user:
            # publish
            if type == '1':
                card.public = True
            # unpublish
            else:
                card.public = False
            card.save()
            # CSRF
            c = RequestContext(request, {})
            t = Template("SUCCESS")
            return HttpResponse(t.render(c), content_type='text/html')
    # CSRF
    c = RequestContext(request, {})
    t = Template("FAIL")
    return HttpResponse(t.render(c), content_type='text/html')