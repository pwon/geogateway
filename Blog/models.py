from django.db import models

from GGUser.models import GGUser

class Notecard(models.Model):
    '''
    represents a log made by a GGUser. HAS fkey to GGUser
    '''

    def __unicode__(self):
        return self.title + " last edited: " + str(self.last_edited)

    user = models.ForeignKey(GGUser)

    title = models.CharField(
        max_length=200,
        blank=False)

    content = models.TextField(
        max_length=1000)

    image = models.ImageField(
        upload_to='notecard_pic',
        editable=True,
        blank=True)

    # True = PUBLIC
    # False = PRIVATE
    public = models.BooleanField(
        default=False)

    last_edited = models.DateTimeField(
        auto_now=True)

class News(models.Model):
    '''
    represents a News content made by an authorized GGUser. HAS fkey to GGUser.
    only superusers can create News content.
    '''

    def __unicode__(self):
        return self.title + " last edited: " + str(self.last_edited)

    title = models.CharField(
        max_length=200,
        blank=False)

    content = models.TextField(
        max_length=2000)

    image = models.ImageField(
        upload_to='news',
        editable=True,
        blank=False)

    last_edited = models.DateTimeField(
        auto_now=True)