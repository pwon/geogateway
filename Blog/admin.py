from django.contrib import admin
from Blog.models import Notecard, News

admin.site.register(Notecard)
admin.site.register(News)