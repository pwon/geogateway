import re

from django import forms
from django.core.exceptions import ObjectDoesNotExist, ValidationError

from GGUser.models import GGUser
from Blog.models import Notecard

class NotecardForm(forms.Form):
    title = forms.CharField(
        max_length=200,
        label=u'Title',
        widget=forms.TextInput({'class': 'form-control', 'placeholder': 'New Notecard Title'}))
    
    content = forms.CharField(
        label=u'Content',
        widget=forms.Textarea({'class': 'form-control', 'placeholder': 'New Notecard Content', 'rows': 4}))

    image = forms.ImageField(
        label='Attach an Image',
        required=False)