from django.db import models

from GGUser.models import GGUser

class Dataset_UAVSAR(models.Model):
	'''
	represents a dataset retrieved for UAVSAR overlay. it contains dataname,
	heading, radardirection, time1, time2, uid and rating
	'''

	def __unicode(self):
		return self.dataname
	
	uid = models.TextField(
		max_length=20) # "uid": "3"

	dataname = models.CharField(
			max_length=100,
			blank=False) # "SanAnd_26530_09010-003_09082-002_0231d_s01_L090HH_01"

	heading = models.TextField(
		max_length=100) # "heading": "-96.076597"

	radardirection = models.CharField(
		max_length=50,
		blank=False) # "radardirection": "Left"

	time1 = models.DateTimeField() # "time1": "16-Oct-2009 16:32:06 UTC"

	time2 = models.DateTimeField() # "time2": "27-Feb-2009 00:43:04 UTC"

	rating = models.TextField(
		max_length=10,
		default=0) # some float between 0 and 10 # 0 means no ratings

class Project(models.Model):
	'''
	represents a ongoing Project started and saved by a GGUser. HAS fkey to GGUser
	'''

	def __unicode__(self):
		return self.title

	user = models.ForeignKey(
		GGUser,
		unique=False)

	title = models.CharField(
		max_length=200,
		blank=False)