import urllib2
import csv
import json

from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.template import RequestContext, Template
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User

from Tools.models import Dataset_UAVSAR, Project

# return uavsar query result to frontend
# request comes in as ajax GET request with querystr in GET data
def uavsar_query(request):
	if request.method == 'GET':
		# UI = University of Indiana
		UIurl = 'http://gf2.ucs.indiana.edu/quaketables/uavsar/search?geometry='
		querystr = request.GET.get('querystr')
		querystr = querystr.replace(" ", "")
		datasets = json.loads(urllib2.urlopen(UIurl + querystr).read())
		response = []
		for dataset in datasets:
			time1 = dataset['time1']
			time2 = dataset['time2']
			obj, created = Dataset_UAVSAR.objects.get_or_create(
				uid=dataset['uid'],
				dataname=dataset['dataname'],
				heading=dataset['heading'],
				radardirection=dataset['radardirection'],
				time1=convertToDatetime("UAVSAR", time1),
				time2=convertToDatetime("UAVSAR", time2))
			rated_dataset = {}
			rated_dataset['uid'] = obj.uid
			rated_dataset['dataname'] = obj.dataname
			rated_dataset['heading'] = obj.heading
			rated_dataset['radardirection'] = obj.radardirection
			rated_dataset['time1'] = time1
			rated_dataset['time2'] = time2
			rated_dataset['rating'] = obj.rating
			response.append(rated_dataset)
		return HttpResponse(json.dumps(response), content_type="application/json")

def convertToDatetime(type, str):
	if type == "UAVSAR":
		split_str = str.split(" ")
		date = split_str[0]
		time = split_str[1]
		tz = split_str[2]
		split_date = date.split("-")
		month_to_num = {
			'Jan': '01',
			'Feb': '02',
			'Mar': '03',
			'Apr': '04',
			'May': '05',
			'Jun': '06',
			'Jul': '07',
			'Aug': '08',
			'Sep': '09',
			'Oct': '10',
			'Nov': '11',
			'Dec': '12'
		}
		day = "%02d" % int(split_date[0])
		tz_to_datetime = {
			'UTC': 'Z'
		}
		tz = tz_to_datetime[tz]
		return split_date[2] + '-' + month_to_num[split_date[1]] + '-' + day \
			+ ' ' + time + tz

def los_query(request):
	if request.method == 'GET':
		base_url = 'http://gf1.ucs.indiana.edu/insartool/profile?image=InSAR:uid'
		image_uid = request.GET.get('image_uid')
		lat1 = request.GET.get('lat1')
		lng1 = request.GET.get('lng1')
		lat2 = request.GET.get('lat2')
		lng2 = request.GET.get('lng2')
		latlng1 = lng1 + ',' + lat1
		latlng2 = lng2 + ',' + lat2
		frmt = request.GET.get('format')
		resolution = request.GET.get('resolution')
		method = request.GET.get('method')
		average = None
		if method == 'average':
			average = request.GET.get('average')
		query_url = base_url + image_uid + '_unw' + '&point=' + \
					latlng1 + "," + latlng2 + '&format=' + frmt + \
					'&resolution=' + resolution + '&method=' + method
		if average is not None:
			query_url += '&average=' + average
		response = urllib2.urlopen(query_url)
		csv_response = csv.reader(response)
		http_response = HttpResponse(content_type='text/csv')
		http_response['Content-Disposition'] = 'attachment; filename="LOS.csv"'
		writer = csv.writer(http_response)
		for row in csv_response:
			writer.writerow([row[-2],row[-1]])
		return http_response

def hgt_query(request):
	if request.method == 'GET':
		base_url = 'http://gf1.ucs.indiana.edu/insartool/profile?image=InSAR:uid'
		image_uid = request.GET.get('image_uid')
		lat1 = request.GET.get('lat1')
		lng1 = request.GET.get('lng1')
		lat2 = request.GET.get('lat2')
		lng2 = request.GET.get('lng2')
		latlng1 = lng1 + ',' + lat1
		latlng2 = lng2 + ',' + lat2
		frmt = request.GET.get('format')
		resolution = request.GET.get('resolution')
		method = request.GET.get('method')
		average = None
		if method == 'average':
			average = request.GET.get('average')
		query_url = base_url + image_uid + '_hgt' + '&point=' + \
					latlng1 + "," + latlng2 + '&format=' + frmt + \
					'&resolution=' + resolution + '&method=' + method
		if average is not None:
			query_url += '&average=' + average
		response = urllib2.urlopen(query_url)
		csv_response = csv.reader(response)
		http_response = HttpResponse(content_type='text/csv')
		http_response['Content-Disposition'] = 'attachment; filename="HGT.csv"'
		writer = csv.writer(http_response)
		for row in csv_response:
			writer.writerow([row[-2],row[-1]])
		return http_response
