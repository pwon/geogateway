from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # media files
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

    # admin page
    url(r'^admin/', include(admin.site.urls)),

    # authentication related links
    url(r'^logout/$', 'GGUser.views.logout_view'),
    url(r'^login/$', 'GGUser.views.login_view'),
    url(r'^signup/$', 'GGUser.views.signup'),
    url(r'^settings_submit/$', 'GGUser.views.settings_submit'),

    # GGUser Profile URLs
    url(r'^$', 'GGUser.views.view_main_smart', name="profile_smart"),
    url(r'^guest/$', 'GGUser.views.view_main_guest', name="profile_guest"),
    url(r'^gguser/(?P<GGUser_id>\d+)/$', 'GGUser.views.view_main'),

    # GGUser Colleague List Retrieval, Follow, Unfollow
    url(r'^gguser/(?P<GGUser_id>\d+)/get_colleagues/$', 'GGUser.views.get_colleagues_ajax'),
    url(r'^gguser/(?P<GGUser_id>\d+)/follow/(?P<followee_id>\d+)/$', 'GGUser.views.follow_colleague_ajax'),
    url(r'^gguser/(?P<GGUser_id>\d+)/unfollow/(?P<followee_id>\d+)/$', 'GGUser.views.unfollow_colleague_ajax'),

    # Blog ajax request handlers
    url(r'^gguser/(?P<GGUser_id>-?\d+)/get_notecards/(?P<start_index>\d+)/(?P<num_cards>\d+)', 'Blog.views.get_notecards_ajax'),
    url(r'^gguser/(?P<GGUser_id>\d+)/post_notecard', 'Blog.views.post_notecard_ajax'),
    url(r'^gguser/(?P<GGUser_id>\d+)/delete_notecard/(?P<card_id>\d+)', 'Blog.views.delete_notecard_ajax'),
    url(r'^gguser/(?P<GGUser_id>\d+)/publish_notecard/(?P<card_id>\d+)/(?P<type>\d)/', 'Blog.views.publish_notecard_ajax'),

    # UAVSAR query request handlers
    url(r'^uavsar_query/$', 'Tools.views.uavsar_query'),
    url(r'^los_query/$', 'Tools.views.los_query')
)
